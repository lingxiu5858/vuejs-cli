# 🔥uni-app 写了一个网易云微信小程序

uni-app、vue、小程序集于一身的实战，很好的一个练手项目。包括：云音乐首页分类、音乐列表页展示、音乐详情页、音乐播放器、推荐音乐、用户留言、音乐搜索、加载骨架屏等实现。
![\[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-q8PTY7F5-1624349745574)(C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210620003255158.png)\]](https://img-blog.csdnimg.cn/20210622161622270.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDAxOTM3MA==,size_16,color_FFFFFF,t_70)

⚡后端接口地址  https://github.com/Binaryify/NeteaseCloudMusicApi

🎉项目下载地址 [ https://github.com/kuishou68/neteaseMusic](https://github.com/kuishou68/neteaseMusic)

![\[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-GJcbKrn3-1624349745576)(C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210622151456623.png)\]](https://img-blog.csdnimg.cn/20210622161716426.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDAxOTM3MA==,size_16,color_FFFFFF,t_70)
![\[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-zkr4GeUd-1624349745577)(C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210622152623425.png)\]](https://img-blog.csdnimg.cn/20210622161828123.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDAxOTM3MA==,size_16,color_FFFFFF,t_70)
![\[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-mYlxiB2q-1624349745579)(C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210622154449521.png)\]](https://img-blog.csdnimg.cn/20210622161845941.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDAxOTM3MA==,size_16,color_FFFFFF,t_70)

说


📣可以拿真实的网易云微信小程序做个对比
![\[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-cikk5UOW-1624349745580)(C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210622134407010.png)\]](https://img-blog.csdnimg.cn/2021062216192544.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDAxOTM3MA==,size_16,color_FFFFFF,t_70)




- 👀开源不易，🙏球球路过的大哥点个Follow、点个Star再走吧🙏！[ https://github.com/kuishou68](https://github.com/kuishou68)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210622161957292.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDAxOTM3MA==,size_16,color_FFFFFF,t_70)

- 🚀项目下载地址  [https://github.com/kuishou68/neteaseMusic](https://github.com/kuishou68/neteaseMusic)
![\[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-Lkhuvxwc-1624349745582)(C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210622160119798.png)\]](https://img-blog.csdnimg.cn/2021062216210397.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDAxOTM3MA==,size_16,color_FFFFFF,t_70)


1.🛰️代码中很多地方都加入了注释，看不懂代码的可以看注释。
![\[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-SfKrchnz-1624349745583)(C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210622130801401.png)\]](https://img-blog.csdnimg.cn/20210622162232877.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDAxOTM3MA==,size_16,color_FFFFFF,t_70)


2.💡其中用到了骨架屏，下载地址  https://ext.dcloud.net.cn/plugin?id=1439![\[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-ZfdyNAIW-1624349745583)(C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210622131410072.png)\]](https://img-blog.csdnimg.cn/20210622162249647.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDAxOTM3MA==,size_16,color_FFFFFF,t_70)


3.🌈两种安装方式，自行选择。
![\[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-fXVymhnd-1624349745584)(C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210622131910935.png)\]](https://img-blog.csdnimg.cn/20210622162312704.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDAxOTM3MA==,size_16,color_FFFFFF,t_70)


4.🎨选择要导入的项目。
![\[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-q1Jvm9mO-1624349745585)(C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210622132127131.png)\]](https://img-blog.csdnimg.cn/20210622162335655.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDAxOTM3MA==,size_16,color_FFFFFF,t_70)

5.💎安装好后  components下会多出个文件
![\[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-38UOTnpi-1624349745586)(C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210622132238210.png)\]](https://img-blog.csdnimg.cn/20210622162403334.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDAxOTM3MA==,size_16,color_FFFFFF,t_70)

------------------------

1.💡还会用到scss/sass编译
![\[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-kP84DIKl-1624349745586)(C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210622132653008.png)\]](https://img-blog.csdnimg.cn/20210622162422511.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDAxOTM3MA==,size_16,color_FFFFFF,t_70)


2.一样的步骤，打开HBuider安装就行。
![\[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-3rgrFbG0-1624349745587)(C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20210622132804809.png)\]](https://img-blog.csdnimg.cn/20210622162433987.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDAxOTM3MA==,size_16,color_FFFFFF,t_70)